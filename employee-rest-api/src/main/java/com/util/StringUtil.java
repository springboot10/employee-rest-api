package com.util;

import java.util.concurrent.atomic.AtomicInteger;

public class StringUtil {
	private static final AtomicInteger sequence = new AtomicInteger(100); 
 
 	public static int next() {
 		return sequence.incrementAndGet();
 	}  
}