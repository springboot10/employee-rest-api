package com.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeBean {
	private Integer id;
	private String name;
	private Long salary;
	
	public EmployeeBean() {}

	public EmployeeBean(Integer id, String name, Long salary) {
		super();
		this.id = id;
		this.name = name;
		this.salary = salary;
	}

}
