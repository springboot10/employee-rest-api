package com.test;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.bean.EmployeeBean;
import com.dao.EmployeeDao;

//run test url =http://<host>:<port>/employee
@RestController
public class EmployeeController {
	
	EmployeeDao dao = new EmployeeDao();

	@RequestMapping("/employee")
	public Object getEmpAll() {
		return dao.getEmployeeAll();
	}
	
	@RequestMapping(value = "/employee/{id}", method = RequestMethod.GET)
	public Object getEmpById(@PathVariable int id) {	   
		return dao.getEmployeeById(id);	
	}

	@RequestMapping(value = "/employee", method = RequestMethod.POST)
	public Object postEmp(@RequestBody EmployeeBean tempEMP ) {	
		return dao.postEmployee(tempEMP);
	}
	
	@RequestMapping(value = "/employee", method = RequestMethod.PUT)
	public Object  putEmp(@RequestBody EmployeeBean data) {	
		return dao.putEmployee(data);
	}
	
	@RequestMapping(value = "/employee/{id}", method = RequestMethod.DELETE)
	public Object  deleteEmp(@PathVariable int id) {
		return dao.deleteEmployee(id);
	}
}
