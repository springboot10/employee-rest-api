package com.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.bean.EmployeeBean;
import com.util.StringUtil;

public class EmployeeDao {
	
	List<EmployeeBean> list = null;
	private static HashMap<Integer, EmployeeBean> mapEMP = mockEmployees();
	
	private static HashMap<Integer, EmployeeBean> mockEmployees(){
		mapEMP = new HashMap<Integer, EmployeeBean>();
		int next1 = StringUtil.next();
		int next2 = StringUtil.next();
		mapEMP.put(next1, new EmployeeBean(next1, "TEST A", 20000L));
		mapEMP.put(next2, new EmployeeBean(next2, "TEST B", 10000L));	
	return mapEMP;	
	}

	public List<EmployeeBean> getEmployeeAll() {
		list = new ArrayList<EmployeeBean>(mapEMP.values());	
		return list;
	}
	
	public Object getEmployeeById(int id) {
		if(mapEMP.containsKey(id)) {
			return mapEMP.get(id);
		}else {
			return "id :"+id+" not found";
		}		
	}
	
	public List<EmployeeBean> postEmployee(EmployeeBean data ) {	
		data.setId(StringUtil.next());
		mapEMP.put(data.getId(), data);
		list = new ArrayList<EmployeeBean>(mapEMP.values());
		return list;
	}
	
	public String  putEmployee(EmployeeBean data) {	
		if(mapEMP.containsKey(data.getId())) {
			mapEMP.remove(data.getId());
			return "update :"+data.getId()+" success!";
		}else {
			return "id :"+data.getId()+" not found";
		}
	}
	
	public String  deleteEmployee(int id) {
		if(mapEMP.containsKey(id)) {
			mapEMP.remove(id);
			return "delete :"+id+" success!";
		}else {
			return "id :"+id+" not found";
		}
	}	
	
}
