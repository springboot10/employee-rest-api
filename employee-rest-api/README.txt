
#### Build jar for deploy to \target\<jarName>.jar ####
$ mvn clean package spring-boot:repackage


#### Build image ####
$ docker build --tag=employee-rest-api:latest .


#### Start containers automatically ####
$ docker run -d --name employee-rest-api -it -p 8080:8080 employee-rest-api:latest

#### Start containers automatically ####
$ docker update --restart unless-stopped employee-rest-api
